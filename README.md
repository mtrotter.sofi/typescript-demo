# typescript-demo

```sh
# a place to put our code
mkdir typescript-demo; cd typescript-demo

# initialize an npm project for dev and runtime dependencies
npm init -y

# install typescript locally
npm i -D typescript

# initialize a default tsconfig.json, tells the compiler (tsc) what to do
npx tsc --init

# but we're going to use our own!
mv tsconfig.json tsconfig.default.json
echo '{
  "compilerOptions": {
    "rootDir": "src",
    "outDir": "dist",
    "target": "ES2021",
    "module": "ES2020",
    "lib": ["ES2021"],
    "moduleResolution": "node",
    "allowSyntheticDefaultImports": true,
    "strict": true
  }
}' > tsconfig.json

# ignore dist and node_modules
echo 'node_modules/
dist/' > .gitignore

# many libs have their typescript type definitions built in, but if you
# find a dependency which doesn't include types they're often available
# with the "@types/" prefix, and type definitions are not used at runtime
npm i -D @types/node @types/react @types/react-dom

# react, because why not
npm i -S react react-dom

# code!
mkdir src
touch src/index.ts
```
