import { z } from "zod";

const User = z.object({
  username: z.string(),
});

// extract the inferred type
type User = z.infer<typeof User>;

User.parse({ username: "Ludwig" });

// fetch lets you specify the shape of whatever it's parsing -- this can be unsafe!
const user: SomeType = await fetch();

// fetch as unknown, then validate:
try {
  const response: unknown = await fetch();
  const user: User = User.parse(response);
} catch (e) {
  // network or parsing error! provide a default/null or fail early
}

// see: https://github.com/colinhacks/zod#basic-usage
