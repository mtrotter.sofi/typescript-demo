///
/// Defining types
///

// Primitives

const firstName = "maddie";

const lastName: string = "trotter";

const maybe: boolean = true;

let age: number = 15234;
age += 1;
// age = "1";

let x: number | null = null;
x = 5;
// x = undefined;

const y: number | null | undefined = ((): number | null | undefined => 27)();

if (y !== null) {
  y.toFixed();
}

if (y !== null && y !== undefined) {
  y.toFixed();
}

if (y != null) {
  y.toFixed();
}

y?.toFixed();

// Objects (record, pojo, etc)

const sofi = { symbol: "SOFI", name: "SoFi Technologies", price: 10.67 };

const tsla: { symbol: string; name: string; price: number } = {
  symbol: "TSLA",
  name: "Tesla",
  price: 809.87,
};

type Security = { symbol: string; name: string; price: number };

const aapl: Security = { symbol: "AAPL", name: "Apple", price: 164.85 };

const securities: Security[] = [sofi, tsla, aapl];

sofi.thisFieldDoesntExist;

// Array

const fruit: Array<string> = ["Kiwi", "Banana", "Blueberry"];

// const mostlyFruit: string[] = [...fruit, new Date()];

// Tuple

const anUnlikelyPair: [string, Date] = ["hey", new Date()];

const thruple: [string, Date, number] = ["hey", new Date(), 7];

const [a, b, c] = thruple;

const [firstFruit, ...otherFruit] = fruit;

// Functions

const add = (x: number, y: number): number => x + y;

function subtract(x: number, y: number): number {
  return x - y;
}

///
/// More objects
///

// type Security = {
//   symbol: string;
//   name: string;
//   price: number
// };

interface Coin {
  symbol: string;
  name: string;
  price: number;
}

interface Coin {
  hrmmmmmm?: "what's this?";
}

const btc: Coin = {
  symbol: "BTC",
  name: "Bitcoin",
  price: NaN,
};

btc.hrmmmmmm;
btc.thisFieldDoesntExist;

// Readonly

type User = {
  readonly username: string;
};

const user: User = { username: "someone" };
user.username = "noone";

type Post = Readonly<{
  body: string;
}>;
const post: Post = { body: "hi." };
post.body = "bye.";

// Exclusive types

const looksLikeAPost = { body: "hi.", createdDate: new Date() };
const actsLikeAPost: Post = looksLikeAPost;

actsLikeAPost.createdDate;
looksLikeAPost.createdDate;

// Intersection

type UserAndPost = User & Post;

const mutant: UserAndPost = { username: "humanforsure", body: "wait what" };

// Union

function isUserOrPost(userOrPost: User | Post): void {
  if ("username" in userOrPost) {
    userOrPost.username;
  } else {
    userOrPost.body;
  }
}

type Shape = Cube | RectangularPrism | Sphere;
type Cube = { kind: "cube"; x: number };
type RectangularPrism = {
  kind: "rectangular-prism";
  x: number;
  y: number;
  z: number;
};
type Sphere = { kind: "sphere"; radius: number };

function area(shape: Sphere): number;
function area(shape: Shape): number {
  switch (shape.kind) {
    case "cube": {
      return 6 * Math.pow(shape.x, 2);
    }
    case "rectangular-prism": {
      return 2 * (shape.x * shape.y + shape.x * shape.z + shape.y * shape.z);
    }
    case "sphere": {
      return 4 * Math.PI * Math.pow(shape.radius, 2);
    }
  }
}

///
/// Baddies
///

// Any

const ok = null;

const alsoOk = () => 9;

const notOk = (x) => 9;

const okButVerySketch = (x: any) => 9 + x;

const s = okButVerySketch(new Date());
s.asdf;

// Unknown (not so bad)

const lessSketch = (x: unknown) => 9 + x;

const lessSketch2 = (x: unknown): number | null =>
  typeof x !== "number" ? null : 9 + x;

export {};
