// consider..
function sort(items) { ... }

//#region ts sort 1

function sort1(items: any[]): void {}

//#endregion

//#region ts sort 2

function sort2(items: Array<any>): Array<any> {}

//#endregion

//#region ts sort 3

function sort3(items: Array<{}>): Array<{}> {
  return Array.from(items).sort((a, b) => a.? > b.?);
}

//#endregion

//#region ts sort 4

function sort4(items: Array<{ order: number }>): Array<{ order: number }> {
  return Array.from(items).sort((a, b) => a.order - b.order);
}

interface Item {
  id: string;
  order: number;
}
const items: Item[] = [{ id: "uqeiofj", order: 2 }, { id: "wefiojk", order: 1 }];
const sortedIds = sort4(items).map(item => item.id);

//#endregion

//#region ts sort 5

function sort5<T extends { order: number }>(items: Array<T>): Array<T> {
  return Array.from(items).sort((a, b) => a.order - b.order);
}

const sortedIds2 = sort5(items).map(item => item.id);

//#endregion

//#region ts sort 6

function sort6<T>(items: Array<T>, by: (item: T) => number): Array<T> {
  return Array.from(items).sort((a, b) => by(a) - by(b));
}

const sortedIds3 = sort6(items, item => item.order).map(item => item.id);

//#endregion

//#region ts sort 6

async function foo() {
  const [a, b, c] = await Promise.all([
    Promise.resolve(5),
    Promise.resolve("hello"),
    Promise.resolve(new Date()),
  ]);
}

//#endregion