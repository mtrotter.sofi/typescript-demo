// function add(   x: number, y: number       ):     number {
//   return x + y;
// }

// function add(   x: number    ):         (y: number) => number         {
//   return y => x + y;
// }

function add(x: number, y: number): number;
function add(x: number): (y: number) => number;
function add(x: number, y?: number): number | ((y: number) => number) {
  if (y == null) {
    return (y) => x + y;
  }

  return x + y;
}

const three = add(1, 2);
const seven = add(3)(4);

export {};
