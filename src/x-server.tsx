import { createServer, IncomingMessage, ServerResponse } from "http";
import { hrtime } from "process";
import * as React from "react";
import { renderToStaticNodeStream } from "react-dom/server.js";

const App = () => <h1>hi from react</h1>;

export const runServer = () => {
  createServer((req, res) => {
    const [startS, startNS] = hrtime();

    res.write(renderToStaticNodeStream(<App />).read(), (err) => {
      if (err) {
        console.error("response write error:", err);
      }
    });

    res.end(() => {
      const [endS, endNS] = hrtime();
      const elapsedMS = (endS - startS) / 1000 + (endNS - startNS) / 1000000;
      console.log(
        `${req.method} ${req.url} from ${
          req.socket.remoteAddress
        } took ${elapsedMS.toFixed(2)}ms`
      );
    });
  }).listen(3000);
};
