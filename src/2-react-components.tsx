interface SomeComponentProps {
  children: React.ReactChildren;
}

interface SomeComponentState {
  counter: number;
}

class SomeComponent extends React.Component<
  SomeComponentProps,
  SomeComponentState
> {
  state = { counter: 0 };

  render() {
    return (
      <div>
        <div>count: {this.state.counter}</div>
        <div>{this.props.children}</div>
      </div>
    );
  }
}

const AnotherComponent: React.FC<{ children: React.ReactChildren }> = ({
  children,
}) => {
  const [counter, setCounter] = React.useState(0);

  return (
    <div>
      <div>count: {counter}</div>
      <div>{children}</div>
    </div>
  );
};
